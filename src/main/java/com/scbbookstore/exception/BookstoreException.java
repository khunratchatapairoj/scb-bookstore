package com.scbbookstore.exception;

public class BookstoreException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String errCode;
	public BookstoreException(String message, String errCode) {
		super(message);
		this.errCode = errCode;
	}
	
	public BookstoreException(String message, String errCode, Throwable e) {
		super(message, e);
		this.errCode = errCode;
	}
	
	public String getErrCode() {
		return this.errCode;
	}
	
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
}