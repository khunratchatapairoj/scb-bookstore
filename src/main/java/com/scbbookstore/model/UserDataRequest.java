package com.scbbookstore.model;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class UserDataRequest {
	private String username;
	
	private String password;
	
	private String name;
	
	private String surname;
	
	private Date date_of_birth;
}
