package com.scbbookstore.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BooksFormPublisherResponse {

    private Long id;
    private String book_name;
    private String author_name;
    private BigDecimal price;

}
