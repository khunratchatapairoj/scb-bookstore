package com.scbbookstore.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class UserOrderBookResponse {
	private BigDecimal price;
}
