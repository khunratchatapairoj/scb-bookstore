package com.scbbookstore.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scbbookstore.component.ErrorMassege;
import com.scbbookstore.exception.BookstoreException;
import com.scbbookstore.model.UserDataRequest;
import com.scbbookstore.model.UserDataResponse;
import com.scbbookstore.model.UserOrderBookRequest;
import com.scbbookstore.model.UserOrderBookResponse;
import com.scbbookstore.service.OrderService;
import com.scbbookstore.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	OrderService orderService;
	
	@GetMapping(headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<UserDataResponse> getUser() {
		UserDataResponse response = usersService.getUser();
		if(response != null) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		throw new BookstoreException("User not found.", ErrorMassege.NO_DATA_FOUND);
	}

	@PostMapping
	public ResponseEntity<?> createUser(@Valid @RequestBody  UserDataRequest request) {
		Long id = usersService.createUser(request);
		if( id != null) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			throw new BookstoreException("Can't create user.", ErrorMassege.CANNOT_SAVE_DATA);
		}
	}

	@DeleteMapping(headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<?> deleteUser() {
		boolean deleteSuccess = usersService.delete();
		if(deleteSuccess) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		throw new BookstoreException("User not found.", ErrorMassege.NO_DATA_FOUND);
	}
	
	@PostMapping(value = "/order", headers = { "Authorization" })
	@PreAuthorize("hasAuthority('USER')")
	public ResponseEntity<UserOrderBookResponse> orderBook(
		@RequestBody UserOrderBookRequest request
	) {
		UserOrderBookResponse response = orderService.save(request);
		if(response != null) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		throw new BookstoreException("Can't save order.", ErrorMassege.CANNOT_SAVE_DATA);
	}
}
