package com.scbbookstore.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scbbookstore.model.BooksDataResponse;
import com.scbbookstore.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {

	@Autowired
	BooksService booksService;
	
	@GetMapping
	  public ResponseEntity<?> getBooks() {
        Map<String, List<BooksDataResponse>> response = new HashMap<>();
        response.put("books", booksService.getBooks());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
}
