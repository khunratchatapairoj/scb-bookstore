package com.scbbookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.scbbookstore.entity.Orders;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long> {

	public List<Orders> findIdByUserId(Long userId); 
	
	@Query("SELECT "
		+ "   orderDetail.bookId "
		+ " FROM Orders orderHeader "
		+ "   JOIN OrderDetail orderDetail ON orderDetail.orderId = orderHeader.id "
		+ " WHERE orderHeader.userId = :userId "
		+ " GROUP BY orderDetail.bookId ")
	public List<Long> findBookIdByUserId(@Param("userId")Long userId);
}
