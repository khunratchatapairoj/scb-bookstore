package com.scbbookstore.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.scbbookstore.entity.Book;
import com.scbbookstore.exception.BookstoreException;
import com.scbbookstore.model.BooksFormPublisherResponse;
import com.scbbookstore.service.BooksService;
import com.scbbookstore.service.PublisherService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JobsSchedule {

	@Autowired
	BooksService booksService;
	
	@Autowired
	PublisherService jobsScheduleService;

	// At 00:15:00am, on every Sunday, every month
	@Scheduled(cron = "0 15 0 ? * SUN")
	public void loadBooksFromJob(){
		
		BooksFormPublisherResponse[] data = jobsScheduleService.callGetListBooksFromPublisher();
		
		List<Book> listBookEntity = new ArrayList<>();
		for(BooksFormPublisherResponse book: data){
			Book bookEntity = new Book();
			bookEntity.setId(book.getId());
			bookEntity.setBookName(book.getBook_name());
			bookEntity.setPrice(book.getPrice());
			bookEntity.setAuthorName(book.getAuthor_name());
			listBookEntity.add(bookEntity);
		}

		List<Book> listUpdateBook = booksService.updateBookList(listBookEntity);

		if(listUpdateBook.equals(listBookEntity)){
			log.info("Update list of book success");
		} else {
			log.error("Update list of book fail");
			throw new BookstoreException("Update list of book fail", ErrorMassege.UPDATE_BOOK_FAIL);
		}
	}

}
