package com.scbbookstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scbbookstore.entity.Book;
import com.scbbookstore.model.BooksDataResponse;
import com.scbbookstore.model.BooksFormPublisherResponse;
import com.scbbookstore.repository.BookRepository;

@Service
@Transactional
public class BooksService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	PublisherService jobsScheduleService;

	public List<BooksDataResponse> getBooks() {

		List<BooksDataResponse> dataList = new ArrayList<>();
		BooksFormPublisherResponse[] recommendList = jobsScheduleService.callGetListRecommendedFromPublisher();
		List<Long> recommendIdList = new ArrayList<>();
		for (BooksFormPublisherResponse book : recommendList) {
			BooksDataResponse data = new BooksDataResponse();
			recommendIdList.add(book.getId());
			data.setId(book.getId());
			data.setName(book.getBook_name());
			data.setAuthor(book.getAuthor_name());
			data.setPrice(book.getPrice());
			data.setIs_recommended(true);
			dataList.add(data);
		}

		List<Book> bookList = bookRepository.findByIdNotIn(recommendIdList);

		for (Book book : bookList) {
			BooksDataResponse data = new BooksDataResponse();
			data.setId(book.getId());
			data.setName(book.getBookName());
			data.setAuthor(book.getAuthorName());
			data.setPrice(book.getPrice());
			data.setIs_recommended(false);
			dataList.add(data);
		}

		return dataList;
	}

	public List<Book> updateBookList(List<Book> bookEntityList) {
		List<Book> bookList = bookRepository.saveAll(bookEntityList);
		return bookList;
	}
}
