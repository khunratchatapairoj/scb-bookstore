package com.scbbookstore.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scbbookstore.component.ErrorMassege;
import com.scbbookstore.entity.Book;
import com.scbbookstore.entity.OrderDetail;
import com.scbbookstore.entity.Orders;
import com.scbbookstore.entity.UserEntity;
import com.scbbookstore.exception.BookstoreException;
import com.scbbookstore.model.UserOrderBookRequest;
import com.scbbookstore.model.UserOrderBookResponse;
import com.scbbookstore.repository.BookRepository;
import com.scbbookstore.repository.OrderRepository;
import com.scbbookstore.repository.UserRepository;

@Service
@Transactional
public class OrderService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	AuthService authService;

	public UserOrderBookResponse save(UserOrderBookRequest request){
		String userName = authService.getTokenData().getUserName();
		Optional<UserEntity> optUser = userRepository.findByUsername(userName);
		if(optUser.isPresent()) {
			List<Book> bookList = bookRepository.findAllById(request.getOrders());
			if(bookList.size() != 0) {
				List<OrderDetail> orderDetailList = new ArrayList<>();
				BigDecimal price = BigDecimal.ZERO;
				for(Book book : bookList) {
					price = price.add(book.getPrice());
					OrderDetail orderDetail = new OrderDetail();
					orderDetail.setBookId(book.getId());
					orderDetailList.add(orderDetail);
				}
				Orders order = new Orders();
				order.setUserId(optUser.get().getId());
				order.setPrice(price);
				order.setOrderDetailList(orderDetailList);
				order = orderRepository.save(order);
				
				UserOrderBookResponse response = new UserOrderBookResponse();
				response.setPrice(order.getPrice());
				return response;
			}
		} else {
			throw new BookstoreException("User not found.", ErrorMassege.NO_DATA_FOUND);
		}
		return null;
	}


}