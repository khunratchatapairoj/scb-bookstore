package com.scbbookstore.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.scbbookstore.component.ErrorMassege;
import com.scbbookstore.exception.BookstoreException;
import com.scbbookstore.model.BooksFormPublisherResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PublisherService {
	
	private static final String BASE_URL = "https://scb-test-book-publisher.herokuapp.com";

	public BooksFormPublisherResponse[] callGetListBooksFromPublisher() {
		String url = BASE_URL + "/books";
		try {
			RestTemplate restTemplate = new RestTemplate();
			log.info("Request " + url);
			ResponseEntity<BooksFormPublisherResponse[]> response = restTemplate.getForEntity(url, BooksFormPublisherResponse[].class);
			return response.getBody();
		} catch (Exception e) {
			throw new BookstoreException("Fail request " + url, ErrorMassege.WHEN_CALL_API, e);
		}
	}
	
	public BooksFormPublisherResponse[] callGetListRecommendedFromPublisher() {
		String url = BASE_URL + "/books/recommendation";
		try {
			RestTemplate restTemplate = new RestTemplate();
			log.info("Request " + url);
			ResponseEntity<BooksFormPublisherResponse[]> response = restTemplate.getForEntity(url, BooksFormPublisherResponse[].class);
			return response.getBody();
		} catch (Exception e) {
			throw new BookstoreException("Fail request " + url, ErrorMassege.WHEN_CALL_API, e);
		}
	}

}
