package com.scbbookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.scbbookstore.component.JobsSchedule;

@SpringBootApplication
@EnableScheduling
public class BookstoreApplication {

	public static void main(String[] args) {
		
		ApplicationContext applicationContext = SpringApplication.run(BookstoreApplication.class, args);
		
		// Startup application run JobsScheduledTasks.
		JobsSchedule jobsSchedule = applicationContext.getBean(JobsSchedule.class);
		jobsSchedule.loadBooksFromJob();;
		
		
	}

}


