package com.scbbookstore.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_bookstore")
@Data
public class UserEntity {

	@Id
	@SequenceGenerator(name="user_bookstore_id_seq",sequenceName="user_bookstore_id_seq", allocationSize = 1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="user_bookstore_id_seq")
	@Column(name="id" , nullable = false, length = 10)
	private Long id;

	@Column(name="username" , unique = true, nullable = false, length = 100)
	private String username;

	@Column(name="password" , nullable = false, length = 500)
	private String password;
	
	@Column(name="name" , nullable = false, length = 100)
	private String name;
	
	@Column(name="surname" , nullable = false, length = 100)
	private String surname;

	@Column(name="date_of_birth" , nullable = false)
	private Date dateOfBirth;

}
