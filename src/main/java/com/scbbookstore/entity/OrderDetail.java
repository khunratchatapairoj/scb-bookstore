package com.scbbookstore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "order_detail")
@Data
public class OrderDetail {

	@Id
	@SequenceGenerator(name = "order_detail_id_seq", sequenceName = "order_detail_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_detail_id_seq")
	@Column(name = "id" , nullable = false, length = 10)
	private Long id;

	@Column(name = "order_id" , nullable = false, length = 10)
	private Long orderId;

	@Column(name = "book_id" , nullable = false, length = 10)
	private Long bookId;

}
