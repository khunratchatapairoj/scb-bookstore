package com.scbbookstore.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "book")
@Data
public class Book {

    @Id
    @Column(name = "id" , nullable = false, length = 10)
    private Long id;

    @Column(name = "book_name" , nullable = false, length = 300)
    private String bookName;

    @Column(name = "author_name" ,nullable = false , length = 300)
    private String authorName;

    @Column(name = "price" , nullable = false)
    private BigDecimal price;

}
