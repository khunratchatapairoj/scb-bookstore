package com.scbbookstore.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "order_bookstore")
@Data
public class Orders {

	@Id
	@SequenceGenerator(name = "order_bookstore_id_seq", sequenceName = "order_bookstore_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_bookstore_id_seq")
	@Column(name = "id" , nullable = false, length = 10)
	private Long id;

	@Column(name = "user_id" , nullable = false, length = 10)
	private Long userId;

	@Column(name = "price" , nullable = false )
	private BigDecimal price;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", referencedColumnName = "id", insertable=true, updatable=true)
	private List<OrderDetail> orderDetailList;

}
