CREATE SCHEMA dev AUTHORIZATION postgres

CREATE TABLE dev.book (
	id int(10) NOT NULL,
	book_name varchar(300) NULL,
	author_name varchar(300) NULL,
	price numeric(12,2) NOT NULL,
	CONSTRAINT book_pkey PRIMARY KEY (id)
);

CREATE TABLE dev.user_bookstore (
	id int(10) NOT NULL,
	username varchar(100) NOT NULL,
	"password" varchar(500) NOT NULL,
	date_of_birth date NULL,
	"name" varchar(100) NULL,
	surname varchar(100) NULL,
	CONSTRAINT user_bookstore_pk PRIMARY KEY (id),
	CONSTRAINT user_bookstore_un UNIQUE (username)
);


CREATE TABLE dev.order_bookstore (
	id int(10) NOT NULL,
	user_id int(10) NOT NULL,
	price numeric(12,2) NULL,
	CONSTRAINT order_bookstore_pk PRIMARY KEY (id),
	CONSTRAINT order_bookstore_user_bookstore_fk FOREIGN KEY (user_id) REFERENCES dev.user_bookstore(id)
);

CREATE TABLE dev.order_detail (
	id int(10) NOT NULL,
	order_id int(10) NULL,
	book_id int(10) NOT NULL,
	CONSTRAINT order_detail_pk PRIMARY KEY (id),
	CONSTRAINT order_detail_order_bookstore_fk FOREIGN KEY (order_id) REFERENCES dev.order_bookstore(id)
);

CREATE SEQUENCE dev.user_bookstore_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;

CREATE SEQUENCE dev.order_bookstore_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;

CREATE SEQUENCE dev.order_detail_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1;
