# scb-bookstore-service

## Create and run database for BookStore Service

### use docker, docker-compose [get started docker](https://www.docker.com/get-started)

- ```cd docker/```
- ```docker-compose up -d```

### view logs

- ```docker-compose logs -f```

### stop database

- ```docker-compose down```


url http://localhost:9090/users

- Method: POST
- Body (form-data)
    - ```username: "thanaphon.singsuk"```
    - ```password: "password"```
    - ```name: "thanakphon"```
		- ```surname: "singsuk"```


## Login app

> url http://localhost:9090/oauth/token

- Method: POST
- Authorization
    - ```Type: Basic Auth```
    - ```Username: clientId```
    - ```Password: clientSecret```
- Body (form-data)
    - ```grant_type: password```
    - ```username: thanaphon.singsuk```
    - ```password: password```
- Response
    - access_token
    - token_type

## Other API

> Set Header request

- ```Authorization: {response.token_type} {response.access_token}```

> Example

- ```Authorization: bearer oujndkJkfjsk8djfhssd...```

## Scheduled Task

> Scheduled is running when startup application and every 00:05 on Sunday for update list book publisher

## [URL - LOCAL]

- Swagger_API = http://localhost:9090/swagger-ui.html
